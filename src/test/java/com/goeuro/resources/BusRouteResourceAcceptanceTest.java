package com.goeuro.resources;

import com.goeuro.BusRouteConfig;
import com.goeuro.dto.RouteAvailabilityResponse;
import com.goeuro.parser.DefaultBusRouteParser;
import com.goeuro.repository.BusRouteRepository;
import com.goeuro.repository.MemoryBusRouteRepository;
import com.goeuro.service.BusRouteService;
import com.goeuro.service.BusRouteServiceImpl;
import io.dropwizard.testing.junit.ResourceTestRule;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import javax.ws.rs.core.GenericType;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class BusRouteResourceAcceptanceTest {
    private static final BusRouteConfig config = new BusRouteConfig();
    private static final BusRouteRepository repository = new MemoryBusRouteRepository(config, new DefaultBusRouteParser());
    private static final BusRouteService service = new BusRouteServiceImpl(config, repository);
    private static final BusRouteResource resource = new BusRouteResource(config, service);


    @ClassRule
    public static final ResourceTestRule rule = ResourceTestRule.builder()
            .addResource(resource)
            .build();

    @Before
    public void setup(){
        repository.initialize("./data/test1.dat");
    }

    @Test
    public void shouldReturn200WithTrueIfDirectRouteExistsFromAtoB(){
        RouteAvailabilityResponse response = rule.getJerseyTest()
                .target("/api/direct")
                .queryParam("dep_sid", "0")
                .queryParam("arr_sid", "4")
                .request()
                .get(new GenericType<RouteAvailabilityResponse>() {
                });

        assertEquals(new Integer(0), response.getDepartureStationId());
        assertEquals(new Integer(4), response.getArrivalStationId());
        assertTrue(response.isDirectRouteAvailable());
    }

    @Test
    public void shouldReturn200WithFalseIfNoDirectRouteExistsFromAtoB(){
        RouteAvailabilityResponse response = rule.getJerseyTest()
                .target("/api/direct")
                .queryParam("dep_sid", "3")
                .queryParam("arr_sid", "2")
                .request()
                .get(new GenericType<RouteAvailabilityResponse>() {
                });

        assertEquals(new Integer(3), response.getDepartureStationId());
        assertEquals(new Integer(2), response.getArrivalStationId());
        assertFalse(response.isDirectRouteAvailable());
    }
}