package com.goeuro.repository;

import com.goeuro.model.graph.Path;
import com.goeuro.parser.DefaultBusRouteParser;
import com.google.common.collect.Sets;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Set;

import static com.google.common.collect.Sets.newHashSet;
import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MemoryBusRouteRepositoryTest {
    private MemoryBusRouteRepository busRouteRepository;

    @Before
    public void setup(){
        this.busRouteRepository = new MemoryBusRouteRepository(null, new DefaultBusRouteParser());
        this.busRouteRepository.initialize("./data/test1.dat");
    }

    @Test
    public void shouldFindPathFromAtoBOnSingleRoute(){
        List<Path> paths = this.busRouteRepository.findPath(2, 4);
        assertEquals(asList(2,3,4), paths.get(0).getNodes().stream().map(n -> n.getStationId()).collect(toList()));
        assertEquals(Sets.newHashSet(0), paths.get(0).getNodes().stream().map(n -> n.getRouteId()).collect(toSet()));
    }

    @Test
    public void shouldFindMultiplePathsFromAtoB(){
        List<Path> paths = this.busRouteRepository.findPath(0, 4);
        assertTrue(2 == paths.size());
        assertEquals(asList(0,6,4), paths.get(0).getNodes().stream().map(n -> n.getStationId()).collect(toList()));
        assertEquals(newHashSet(2), paths.get(0).getNodes().stream().map(n -> n.getRouteId()).collect(toSet()));
        assertEquals(asList(0,1,2,3,4), paths.get(1).getNodes().stream().map(n -> n.getStationId()).collect(toList()));
        assertEquals(newHashSet(0), paths.get(1).getNodes().stream().map(n -> n.getRouteId()).collect(toSet()));
    }

    @Test
    public void shouldCreateAdjacencyListGraphForAllStations(){
        assertEquals(newHashSet(1, 6), getAdjacentStations(0));
        assertEquals(newHashSet(2, 6), getAdjacentStations(1));
        assertEquals(newHashSet(3), getAdjacentStations(2));
        assertEquals(newHashSet(4, 1), getAdjacentStations(3));
        assertEquals(newHashSet(), getAdjacentStations(4));
        assertEquals(newHashSet(), getAdjacentStations(5));
        assertEquals(newHashSet(5, 4), getAdjacentStations(6));
    }

    private Set<Integer> getAdjacentStations(int stationId) {
        return this.busRouteRepository.nodes.get(stationId).stream()
                .flatMap(node -> node.getAdjacentNodes().stream())
                .map(node -> node.getStationId())
                .collect(toSet());
    }
}