package com.goeuro.parser;

import com.goeuro.exception.InvalidRouteException;
import com.goeuro.exception.InvalidStationException;
import com.goeuro.model.Route;
import com.goeuro.model.Station;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;
import static org.junit.Assert.assertEquals;

public class DefaultBusRouteParserTest {
    private DefaultBusRouteParser busRouteParser;

    @Before
    public void setUp() throws Exception {
        this.busRouteParser = new DefaultBusRouteParser();
    }

    @Test
    public void shouldParseRouteAndStations(){
        Pair<Route, List<Station>> pair = busRouteParser.parseRoute("1 10 20 30");
        Route route = pair.getKey();
        List<Station> stations = pair.getValue();
        assertEquals(new Integer(1),  route.getId());
        assertEquals(3, stations.size());
        assertEquals(asList(10, 20, 30), stations.stream().map(s -> s.getId()).collect(toList()));

    }

    @Test(expected = InvalidRouteException.class)
    public void shouldRaiseErrorIfRouteContainsSameStationMoreThanOnce(){
        Pair<Route, List<Station>> pair = busRouteParser.parseRoute("1 10 10 10");
    }

}