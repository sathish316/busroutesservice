package com.goeuro.service;

import com.goeuro.model.graph.Path;
import com.goeuro.model.graph.StationNode;
import com.goeuro.repository.BusRouteRepository;
import com.google.common.collect.Sets;
import org.junit.Before;
import org.junit.Test;

import java.util.Set;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class BusRouteServiceImplTest {
    private BusRouteRepository repository;
    private BusRouteService busRouteService;

    @Before
    public void setup(){
        repository = mock(BusRouteRepository.class);
        busRouteService = new BusRouteServiceImpl(null, repository);
    }

    @Test
    public void findDirectRoutesShouldReturnDirectRouteIds(){
        int start = 20;
        int end = 40;
        Path path = Path.builder()
                .nodes(asList(
                        StationNode.builder().stationId(20).routeId(1).build(),
                        StationNode.builder().stationId(30).routeId(1).build(),
                        StationNode.builder().stationId(40).routeId(1).build()))
                .build();
        when(repository.findPath(start, end)).thenReturn(asList(path));
        Set<Integer> directRoutes = busRouteService.findDirectRoutes(start, end);

        assertEquals(Sets.newHashSet(1), directRoutes);
    }
}