package com.goeuro.repository;

import com.goeuro.model.graph.Path;

import java.util.List;

public interface BusRouteRepository {
    void initialize(String filePath);

    List<Path> findPath(Integer departureStationId, Integer arrivalStationId);
}
