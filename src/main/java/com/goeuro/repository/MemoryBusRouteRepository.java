package com.goeuro.repository;

import com.goeuro.BusRouteConfig;
import com.goeuro.exception.InvalidRouteException;
import com.goeuro.exception.InvalidStationException;
import com.goeuro.model.Route;
import com.goeuro.model.Station;
import com.goeuro.model.graph.Path;
import com.goeuro.model.graph.StationNode;
import com.goeuro.parser.BusRouteParser;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.tuple.Pair;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class MemoryBusRouteRepository implements BusRouteRepository {
    private BusRouteConfig config;
    private BusRouteParser busRouteParser;
    private Map<Integer, Route> routes = new HashMap<>();
    private Map<Integer, Station> stations = new HashMap<>();
    Map<Integer, List<StationNode>> nodes = new HashMap<>();

    public MemoryBusRouteRepository(BusRouteConfig config, BusRouteParser busRouteParser) {
        this.config = config;
        this.busRouteParser = busRouteParser;
    }

    public void initialize(String filePath) {
        loadRoutes(filePath);
        createGraph();
    }

    /**
     * Find all paths from departureStationId to arrivalStationId using BFS
     *
     * @param departureStationId
     * @param arrivalStationId
     * @return
     */
    @Override
    public List<Path> findPath(Integer departureStationId, Integer arrivalStationId) {
        List<Path> paths = new ArrayList<>();
        Queue<Path> queue = new LinkedList<>();
        //validateStations(departureStationId, arrivalStationId);
        List<StationNode> startNodes = nodes.get(departureStationId);
        if(startNodes != null) {
            for (StationNode startNode : startNodes) {
                queue.add(new Path(startNode));
            }
            while (!queue.isEmpty()) {
                Path path = queue.poll();
                StationNode node = path.getLastNode();
                for (StationNode childNode : node.getAdjacentNodes()) {
                    if (!path.isVisited(childNode.getStationId())) {
                        Path newPath = new Path(path);
                        newPath.add(childNode);
                        queue.add(newPath);
                        if (childNode.getStationId().equals(arrivalStationId)) {
                            paths.add(newPath);
                        }
                    }
                }
            }
        }
        return paths;
    }

    private void validateStations(Integer departureStationId, Integer arrivalStationId) {
        if(!nodes.containsKey(departureStationId)){
            throw new InvalidStationException(departureStationId + " is invalid");
        }
        if(!nodes.containsKey(arrivalStationId)){
            throw new InvalidStationException(arrivalStationId + " is invalid");
        }
    }

    /**
     * Parse routes data file and update routes map, stations map
     *
     * @param path
     */
    private void loadRoutes(String path) {
        try {
            BufferedReader reader = Files.newBufferedReader(Paths.get(path));
            String line;
            int n = Integer.parseInt(reader.readLine());
            while((line = reader.readLine()) != null){
                Pair<Route, List<Station>> pair = busRouteParser.parseRoute(line);
                Route route = pair.getKey();
                List<Station> stationList = pair.getValue();
                this.routes.put(route.getId(), route);
                for (Station station : stationList) {
                    Integer stationId = station.getId();
                    this.stations.putIfAbsent(stationId, station);
                    route.addStation(this.stations.get(stationId));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw new InvalidRouteException(e.getMessage());
        }
    }

    /**
     * Create graph of all stations connected by one or more routes
     */
    private void createGraph() {
        for (Route route: routes.values()){
            StationNode prevNode= null;
            for(Station station: route.getStations()){
                Integer stationId = station.getId();
                StationNode node = new StationNode(stationId, route.getId());
                if(nodes.get(stationId) == null){
                    nodes.put(stationId, Lists.newArrayList(node));
                } else {
                    nodes.get(stationId).add(node);
                }
                if(prevNode != null){
                    prevNode.append(node);
                }
                prevNode = node;
            }
        }

    }
}
