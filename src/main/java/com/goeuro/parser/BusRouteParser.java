package com.goeuro.parser;

import com.goeuro.model.Route;
import com.goeuro.model.Station;
import org.apache.commons.lang3.tuple.Pair;

import java.util.List;

public interface BusRouteParser {

    Pair<Route, List<Station>> parseRoute(String line);
}
