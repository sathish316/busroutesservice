package com.goeuro.parser;

import com.goeuro.exception.InvalidRouteException;
import com.goeuro.model.Route;
import com.goeuro.model.Station;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Slf4j
public class DefaultBusRouteParser implements BusRouteParser {
    @Override
    public Pair<Route, List<Station>> parseRoute(String line) {
        String[] fields = line.split(" ");
        Integer routeID = Integer.parseInt(fields[0]);
        Route route = new Route(routeID);
        List<Station> stations = new ArrayList<>();
        for (int i = 1; i < fields.length; i++) {
            Station station = new Station(Integer.parseInt(fields[i]));
            if(stations.contains(station)) {
                throw new InvalidRouteException("Duplicate station found for route:" + route.getId());
            }
            stations.add(station);
        }
        log.info("Parsing route " + route.getId() + " with stations " + stations);
        return ImmutablePair.of(route, stations);
    }
}
