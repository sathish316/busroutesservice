package com.goeuro.resources;

import com.codahale.metrics.annotation.Timed;
import com.goeuro.BusRouteConfig;
import com.goeuro.dto.RouteAvailabilityResponse;
import com.goeuro.exception.BusRouteFetchException;
import com.goeuro.service.BusRouteService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Set;

@Path("/api")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class BusRouteResource {
    private BusRouteConfig config;
    private BusRouteService busRouteService;

    public BusRouteResource(BusRouteConfig config, BusRouteService busRouteService) {
        this.config = config;
        this.busRouteService = busRouteService;
    }

    @GET
    @Path(("/direct"))
    @Timed
    public Response getPrice(@QueryParam("dep_sid") Integer departureStationId, @QueryParam("arr_sid") Integer arrivalStationId){
        try {
            Set<Integer> routes = busRouteService.findDirectRoutes(departureStationId, arrivalStationId);
            return Response.ok(createResponse(departureStationId, arrivalStationId, routes)).build();

        } catch (Exception e){
            throw new BusRouteFetchException(e);
        }
    }

    private RouteAvailabilityResponse createResponse(Integer departureStationId, Integer arrivalStationId, Set<Integer> routes) {
        return new RouteAvailabilityResponse(departureStationId, arrivalStationId, !routes.isEmpty());
    }
}
