package com.goeuro;

import com.goeuro.parser.BusRouteParser;
import com.goeuro.parser.DefaultBusRouteParser;
import com.goeuro.repository.MemoryBusRouteRepository;
import com.goeuro.resources.BusRouteResource;
import com.goeuro.service.BusRouteService;
import com.goeuro.service.BusRouteServiceImpl;
import io.dropwizard.Application;
import io.dropwizard.setup.Environment;

public class BusRouteApplication extends Application<BusRouteConfig> {
    @Override
    public String getName() {
        return "busRouteService";
    }

    @Override
    public void run(BusRouteConfig config, Environment environment) throws Exception {
        BusRouteParser busRouteParser = new DefaultBusRouteParser();
        MemoryBusRouteRepository busRouteRepository = new MemoryBusRouteRepository(config, busRouteParser);
        busRouteRepository.initialize(config.getRoutesFile());
        BusRouteService busRouteService = new BusRouteServiceImpl(config, busRouteRepository);
        BusRouteResource resource = new BusRouteResource(config, busRouteService);
        environment.jersey().register(resource);
    }

    public static void main(String[] args) throws Exception {
        new BusRouteApplication().run(args);
    }
}
