package com.goeuro.service;

import java.util.Set;

public interface BusRouteService {
    Set<Integer> findDirectRoutes(Integer departureStationId, Integer arrivalStationId);
}
