package com.goeuro.service;

import com.goeuro.BusRouteConfig;
import com.goeuro.model.graph.Path;
import com.goeuro.repository.BusRouteRepository;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class BusRouteServiceImpl implements BusRouteService {
    private BusRouteConfig config;
    private BusRouteRepository busRouteRepository;

    public BusRouteServiceImpl(BusRouteConfig config, BusRouteRepository busRouteRepository) {
        this.config = config;
        this.busRouteRepository = busRouteRepository;
    }

    /**
     * Find all paths from departureStationId to arrivalStationId.
     * Path has direct route if all nodes in path are connected by a common route id.
     *
     * @param departureStationId
     * @param arrivalStationId
     * @return
     */
    @Override
    public Set<Integer> findDirectRoutes(Integer departureStationId, Integer arrivalStationId) {
        List<Path> paths = busRouteRepository.findPath(departureStationId, arrivalStationId);
        Set<Integer> directRoutes = new HashSet<>();
        if(!paths.isEmpty()) {
            for (Path path : paths) {
                Integer directRoute = path.getDirectRoute();
                if(directRoute != null) {
                    directRoutes.add(directRoute);
                }
            }
        }
        return directRoutes;
    }
}
