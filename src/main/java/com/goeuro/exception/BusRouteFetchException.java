package com.goeuro.exception;

import javax.ws.rs.WebApplicationException;

public class BusRouteFetchException extends WebApplicationException {
    public BusRouteFetchException(Exception e) {
        super(e);
    }
}
