package com.goeuro.exception;

import javax.ws.rs.WebApplicationException;

public class InvalidStationException extends WebApplicationException {
    public InvalidStationException(String message) {
        super(message);
    }
}
