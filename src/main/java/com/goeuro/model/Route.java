package com.goeuro.model;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@EqualsAndHashCode(of = "id")
@ToString
public class Route {
    private Integer id;
    private List<Station> stations;

    public Route(Integer id) {
        this.id = id;
        this.stations = new ArrayList<>();
    }

    public void addStation(Station station) {
        this.stations.add(station);
    }
}
