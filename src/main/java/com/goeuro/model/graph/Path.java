package com.goeuro.model.graph;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.util.*;

import static java.util.stream.Collectors.toSet;

@Getter
@Builder
@AllArgsConstructor
public class Path {
    private List<StationNode> nodes;
    private Set<Integer> visitedNodes;

    public Path(StationNode node) {
        this.nodes = new ArrayList<>();
        this.visitedNodes = new HashSet<>();
        this.nodes.add(node);
        this.visitedNodes.add(node.getStationId());
    }

    public Path(Path path) {
        this.nodes = new ArrayList<>(path.getNodes());
        this.visitedNodes = path.getNodes().stream().map(n -> n.getStationId()).collect(toSet());
    }

    public StationNode getLastNode() {
        return nodes.get(nodes.size()-1);
    }

    public void add(StationNode node) {
        this.nodes.add(node);
        this.visitedNodes.add(node.getStationId());
    }

    public boolean isVisited(Integer stationId) {
        return visitedNodes.contains(stationId);
    }

    public Integer getDirectRoute() {
        Integer routeId = null;
        for (StationNode node : nodes) {
            if(routeId == null){
                routeId = node.getRouteId();
            } else if (routeId != node.getRouteId()) {
                return null;
            }
        }
        return routeId;
    }
}
