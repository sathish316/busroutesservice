package com.goeuro.model.graph;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Getter
@EqualsAndHashCode(exclude = {"adjacentNodes"})
@ToString(of = {"stationId", "routeId"})
@Builder
@AllArgsConstructor
public class StationNode {
    private Integer stationId;
    private Integer routeId;
    private List<StationNode> adjacentNodes;

    public StationNode(Integer stationId, Integer routeId) {
        this.stationId = stationId;
        this.routeId = routeId;
        this.adjacentNodes = new ArrayList<>();
    }

    public void append(StationNode node) {
        this.adjacentNodes.add(node);
    }
}
